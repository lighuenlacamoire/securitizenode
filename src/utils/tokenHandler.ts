import { sign, SignOptions, verify, VerifyOptions } from "jsonwebtoken";
import { LoginResponseDTO, TokenPayload } from "../interfaces/models";

//import * as fs from 'fs';
//import * as path from 'path';

const jwtSecretKey: string = process.env.JWT_SECRET_KEY ?? "123456";

const signInOptions: SignOptions = {
  // RS256 uses a public/private key pair. The API provides the private key
  // to generate the JWT. The client gets a public key to validate the
  // signature
  algorithm: "HS512",
  expiresIn: "1h",
};

export const ExtractToken = (header?: string) => {
  let jwt = header;
  // remove Bearer if using Bearer Authorization mechanism
  if (jwt && jwt.toLowerCase().startsWith("bearer")) {
    jwt = jwt.slice("bearer".length).trim();
  }
  return jwt;
};

/**
 * generates JWT used for local testing
 */
export const GenerateToken = (model: LoginResponseDTO) => {
  const payload: TokenPayload = {
    username: model.username,
    email: model.email,
    sub: model.email,
  };

  return sign(payload, jwtSecretKey, signInOptions);
};

/**
 * checks if JWT token is valid
 * @param {string} token the expected token payload
 */
export const ValidateToken = (token: string): Promise<TokenPayload> => {
  /*
  const publicKey = fs.readFileSync(
    path.join(__dirname, './../../../public.key'),
  );
  */
  const verifyOptions: VerifyOptions = {
    algorithms: ["HS512"],
  };
  return new Promise((resolve, reject) => {
    try {
      const decoded = verify(
        token,
        jwtSecretKey,
        verifyOptions,
      ) as TokenPayload;
      if (decoded && decoded.username) {
        return resolve(decoded);
      }
      return reject("Falloooo");
    } catch (error) {
      return reject(error);
    }
  });
  /*
  return new Promise((resolve, reject) => {
    verify(token, publicKey, verifyOptions, (error, decoded: TokenPayload) => {
      if (error) return reject(error);
      resolve(decoded);
    });
  });
  */
};
