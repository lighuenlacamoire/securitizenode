import { Request, Response, NextFunction } from "express";
import { ExtractToken, ValidateToken } from "../utils/tokenHandler";

/**
 * middleware to check whether user has access to a specific endpoint
 *
 * @param allowedAccessTypes list of allowed access types of a specific endpoint
 */
export const Authorization =
  (allowedAccessTypes: string[]) =>
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const jwt = ExtractToken(req.headers.authorization);

      // verify request has token
      if (!jwt) {
        return res.status(401).json({ message: "No fue enviado el JWT token" });
      }

      /*
      // remove Bearer if using Bearer Authorization mechanism
      if (jwt.toLowerCase().startsWith('bearer')) {
        jwt = jwt.slice('bearer'.length).trim();
      }
      */
      // verify token hasn't expired yet
      const decodedToken = await ValidateToken(jwt);

      if (!decodedToken || !decodedToken.username) {
        return res
          .status(401)
          .json({ message: "No posee permisos para acceder a este endpoint" });
      }
      /*
      const hasAccessToEndpoint = allowedAccessTypes.some(at =>
        decodedToken.accessTypes.some(uat => uat === at),
      );

      if (!hasAccessToEndpoint) {
        return res
          .status(401)
          .json({ message: 'No posee permisos para acceder a este endpoint' });
      }
      */

      next();
    } catch (error: any) {
      let newerr = { ...error, message: "Acceso no autorizado" };
      if (error.name === "TokenExpiredError") {
        newerr.message = "Expired token";
      }

      res.status(401).json(newerr);
      return;
    }
  };
