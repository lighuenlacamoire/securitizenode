import http from "http";
import express, { Express, Request, Response, NextFunction } from "express";
import morgan from "morgan";
import cors from "cors";
import dotenv from "dotenv";
import path from "path";
import addressRoutes from "./routes/addressRoutes";
import accountRoutes from "./routes/accountRoutes";

dotenv.config({ path: path.join(__dirname, "..", ".env") });
const app: Express = express();

/** Logging */
app.use(morgan("dev"));
/** Takes care of JSON data */
app.use(express.json());
/** Parse the request */
app.use(express.urlencoded({ extended: true }));

app.use(
  cors({
    origin: "*",
  }),
);
/** RULES OF OUR API */
app.use((req: Request, res: Response, next: NextFunction) => {
  // set the CORS policy
  res.header("Access-Control-Allow-Origin", "*");
  // set the CORS headers
  res.header(
    "Access-Control-Allow-Headers",
    "origin, X-Requested-With,Content-Type,Accept, Authorization",
  );
  // set the CORS method headers
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "GET PATCH DELETE POST");
    return res.status(200).json({});
  }
  next();
});
/** Routes */
app.use("/api/Account", accountRoutes);
app.use("/api/Address", addressRoutes);

/** Error handling */
app.use((req: Request, res: Response, next: NextFunction) => {
  const error = new Error("No encontrado");
  return res.status(404).json({
    message: error.message,
  });
});

/** Server */
const PORT: any = process.env.PORT ?? 5001;
app.listen(PORT, () => console.log(`The server is running on port ${PORT}`));
/*
const httpServer = http.createServer(app);
httpServer.listen(PORT, () =>
  console.log(`The server is running on port ${PORT}`),
);
*/
