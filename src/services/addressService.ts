import { ApiResponseDTO, TransactionDTO } from "../interfaces/models";
import { ApiCall, etherscanApi } from "./api";

const GetEtherBalanceByAddress = async (address: string) =>
  ApiCall<ApiResponseDTO<string>>(
    etherscanApi.account.GetEtherBalanceforSingle(address),
    "GET",
  );

const GetAllNormalTransactions = async (address: string) =>
  ApiCall<ApiResponseDTO<TransactionDTO[]>>(
    etherscanApi.account.GetAllNormalTransactions(address),
    "GET",
  );

export default { GetEtherBalanceByAddress, GetAllNormalTransactions };
