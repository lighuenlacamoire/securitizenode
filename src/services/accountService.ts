import { AxiosError } from "axios";
import { LoginRequestDTO, LoginResponseDTO } from "../interfaces/models";
import { GenerateToken } from "../utils/tokenHandler";

const Login = async (model: LoginRequestDTO): Promise<LoginResponseDTO> => {
  return new Promise((resolve, reject) => {
    // eslint-disable-next-line no-useless-catch
    try {
      if (
        model.username &&
        model.username == process.env.AUTH_USER &&
        model.password &&
        model.password == process.env.AUTH_PASS
      ) {
        const response: LoginResponseDTO = {
          username: model.username,
          email: "usuario@gmail.com",
          token: "",
        };
        response.token = GenerateToken(response);
        return resolve(response);
      } else {
        return reject(new AxiosError("Usuario o contraseña invalidos", "401"));
      }
    } catch (err) {
      return reject(new AxiosError(`${err}`, "500"));
    }
  });
};

export default { Login };
