/* eslint-disable no-useless-catch */
import axios, { AxiosRequestConfig, Method } from "axios";
import { Header } from "../interfaces/services";

const config = {
  baseURL: "https://api.etherscan.io/",
};

export const axiosInstance = axios.create(config);

/**
 * API Calls Handler
 * @param {string} endpoint Endpoint path
 * @param {string} method HTTP Method
 * @param {Header?} header Headers si los necesitase
 * @param {unknown?} body Request body
 * @param {unknown?} parameters Query params
 * @returns {T} respuesta del servicio
 */
export const ApiCall = async <T>(
  endpoint: string,
  method: string,
  header?: Header,
  body?: unknown,
  parameters?: unknown,
): Promise<T> => {
  try {
    const requestConfig: AxiosRequestConfig = {
      method: method as Method,
      url: `${endpoint}&apikey=${process.env.API_KEY}`,
      data: body,
      params: parameters,
      headers: header,
    };
    console.log("Service", requestConfig.url);
    const response = await axiosInstance.request<T>(requestConfig);
    return response?.data;
  } catch (err) {
    throw err;
  }
};

export const etherscanApi = {
  account: {
    GetEtherBalanceforSingle: (address: string) =>
      `api?module=account&action=balance&address=${address}&tag=latest`,
    GetAllNormalTransactions: (address: string) =>
      `api?module=account&action=txlist&address=${address}&startblock=0&endblock=99999999&page=1&offset=10&sort=asc`,
  },
};
