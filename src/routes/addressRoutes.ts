import express from "express";
import Controller from "../controllers/addressController";
import * as Auth from "../utils/authorizationAttribute";

const router = express.Router();

router.get(
  `/:id`,
  Auth.Authorization(["getTeams"]),
  Controller.GetEtherBalanceByAddress,
);

router.get(
  `/:id/Transactions`,
  Auth.Authorization(["getTeams"]),
  Controller.GetAllNormalTransactions,
);

export = router;
