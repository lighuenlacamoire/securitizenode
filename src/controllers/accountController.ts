import { Response, NextFunction } from "express";
import { TypedRequest } from "../interfaces/services";
import { LoginRequestDTO, LoginResponseDTO } from "../interfaces/models";
import accountService from "../services/accountService";
import { AxiosError, isAxiosError } from "axios";

const Login = async (
  req: TypedRequest<LoginRequestDTO>,
  res: Response<LoginResponseDTO | AxiosError>,
  next: NextFunction,
) => {
  try {
    const response = await accountService.Login(req.body);
    res.status(200).json(response);
  } catch (err: any) {
    if (isAxiosError(err)) {
      const code = parseInt(err.code || "500", 10);
      res.status(code).json(err);
    } else {
      res.status(500).json(err);
    }
  }
  return;
};

export default { Login };
