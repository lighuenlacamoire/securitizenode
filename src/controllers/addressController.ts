import { Request, Response, NextFunction } from "express";
import accountService from "../services/addressService";

const GetEtherBalanceByAddress = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const data = await accountService.GetEtherBalanceByAddress(req.params.id);
  return res.status(200).json({ balance: data.result });
};

const GetAllNormalTransactions = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const data = await accountService.GetAllNormalTransactions(req.params.id);
  return res.status(200).json(data.result);
};

export default { GetEtherBalanceByAddress, GetAllNormalTransactions };
