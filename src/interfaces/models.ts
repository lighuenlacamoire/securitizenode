export interface ApiResponseDTO<T> {
  status: string;
  message: string;
  result: T;
}
export interface TransactionDTO {
  blockNumber: string;
  timeStamp: string;
  hash: string;
  nonce: string;
  blockHash: string;
  transactionIndex: string;
  from: string;
  to: string;
  value: string;
  gas: string;
  gasPrice: string;
  isError: string;
  txreceipt_status: string;
  input: string;
  contractAddress: string;
  cumulativeGasUsed: string;
  gasUsed: string;
  confirmations: string;
  methodId: string;
  functionName: string;
}

export interface TokenPayload {
  email: string;
  username: string;
  sub: string;
}
export interface LoginRequestDTO {
  username: string;
  password: string;
}
export interface LoginResponseDTO {
  username: string;
  email: string;
  token: string;
}
