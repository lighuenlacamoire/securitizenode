import { Request } from "express";
export interface Header {
  [key: string]: string;
}

export interface TypedRequest<T> extends Request {
  body: T;
}
